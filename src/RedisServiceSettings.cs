﻿using System;
using System.Net;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis
{
	[Serializable]
	public class RedisServiceSettings
	{
		#region 成员字段

		private StackExchange.Redis.ConfigurationOptions _options;

		#endregion

		#region 构造方法

		public RedisServiceSettings(StackExchange.Redis.ConfigurationOptions options)
		{
			if(options == null)
				throw new ArgumentNullException(nameof(options));

			_options = options;
		}

		#endregion

		#region 公共属性

		public IList<EndPoint> Addresses
		{
			get
			{
				return _options.EndPoints;
			}
		}

		public string Password
		{
			get
			{
				return _options.Password;
			}
		}

		public int DatabaseId
		{
			get
			{
				return _options.DefaultDatabase ?? 0;
			}
		}

		public int ConnectionRetries
		{
			get
			{
				return _options.ConnectRetry;
			}
		}

		public TimeSpan ConnectionTimeout
		{
			get
			{
				return TimeSpan.FromMilliseconds(_options.ConnectTimeout);
			}
		}

		public TimeSpan OperationTimeout
		{
			get
			{
				return TimeSpan.FromMilliseconds(_options.ResponseTimeout);
			}
		}

		public bool UseTwemproxy
		{
			get
			{
				return _options.Proxy == StackExchange.Redis.Proxy.Twemproxy;
			}
		}

		public bool AdvancedModeEnabled
		{
			get
			{
				return _options.AllowAdmin;
			}
		}

		public bool DnsEnabled
		{
			get
			{
				return _options.ResolveDns;
			}
		}

		public bool SslEnabled
		{
			get
			{
				return _options.Ssl;
			}
		}

		public string SslHost
		{
			get
			{
				return _options.SslHost;
			}
		}

		public string ClientName
		{
			get
			{
				return _options.ClientName;
			}
		}

		public string ServiceName
		{
			get
			{
				return _options.ServiceName;
			}
		}

		#endregion

		#region 内部属性

		internal StackExchange.Redis.ConfigurationOptions InnerOptions
		{
			get
			{
				return _options;
			}
		}

		#endregion

		#region 重写方法

		public override string ToString()
		{
			return _options.ToString();
		}

		#endregion
	}
}