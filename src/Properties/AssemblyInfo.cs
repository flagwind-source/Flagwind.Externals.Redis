﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Externals.Redis")]
[assembly: AssemblyDescription("This is a library about Redis SDK.")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind.Externals.Redis Library")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]

[assembly: ComVisible(false)]
[assembly: Guid("0796de7c-8d92-45e9-bd75-ec48f1035e68")]
[assembly: AssemblyVersion("1.3.3.623")]
[assembly: AssemblyFileVersion("1.3.3.623")]
