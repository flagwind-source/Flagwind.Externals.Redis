﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;

namespace Flagwind.Externals.Redis
{
	public class RedisHashset : RedisObjectBase, IRedisHashset, ICollection, IList, ICollection<string>
	{
		#region 私有变量

		private readonly object _syncRoot;

		#endregion

		#region 构造方法

		public RedisHashset(string name, StackExchange.Redis.IDatabase database) : base(name, database)
		{
			_syncRoot = new object();
		}

		#endregion

		#region 公共属性

		public int Count
		{
			get
			{
				return (int)this.Database.SetLength(this.Name);
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		#endregion

		#region 公共方法

		public HashSet<string> GetExcept(params string[] other)
		{
			return new HashSet<string>(this.Database.SetCombine(SetOperation.Difference, this.GetRedisKeys(other)).ToStringArray());
		}

		public long SetExcept(string destination, params string[] other)
		{
			return this.Database.SetCombineAndStore(SetOperation.Difference, destination, this.GetRedisKeys(other));
		}

		public HashSet<string> GetIntersect(params string[] other)
		{
			return new HashSet<string>(this.Database.SetCombine(SetOperation.Intersect, this.GetRedisKeys(other)).ToStringArray());
		}

		public long SetIntersect(string destination, params string[] other)
		{
			return this.Database.SetCombineAndStore(SetOperation.Intersect, destination, this.GetRedisKeys(other));
		}

		public HashSet<string> GetUnion(params string[] other)
		{
			return new HashSet<string>(this.Database.SetCombine(SetOperation.Union, this.GetRedisKeys(other)).ToStringArray());
		}

		public long SetUnion(string destination, params string[] other)
		{
			return this.Database.SetCombineAndStore(SetOperation.Union, destination, this.GetRedisKeys(other));
		}

		public HashSet<string> GetRandomValues(int count)
		{
			return new HashSet<string>(this.Database.SetRandomMembers(this.Name, count).ToStringArray());
		}

		public bool Move(string destination, string item)
		{
			return this.Database.SetMove(this.Name, destination, item);
		}

		public int RemoveRange(params string[] items)
		{
			return (int)this.Database.SetRemove(this.Name, items.ToRedisValues());
		}

		public bool Remove(string item)
		{
			return this.Database.SetRemove(this.Name, item);
		}

		public void Add(string item)
		{
			this.Database.SetAdd(this.Name, item);
		}

		public int AddRange(IEnumerable<string> items)
		{
			if(items == null)
				return 0;

			return (int)this.Database.SetAdd(this.Name, items.ToRedisValues());
		}

		public int AddRange(params string[] items)
		{
			return this.AddRange((IEnumerable<string>)items);
		}

		public void Clear()
		{
			this.Database.KeyDelete(this.Name);
		}

		public bool Contains(string item)
		{
			return this.Database.SetContains(this.Name, item);
		}

		#endregion

		#region 显式实现

		bool ICollection.IsSynchronized
		{
			get
			{
				return true;
			}
		}

		object ICollection.SyncRoot
		{
			get
			{
				return _syncRoot;
			}
		}

		void ICollection.CopyTo(Array array, int index)
		{
			var items = this.Database.SetMembers(this.Name);

			if(items != null && items.Length > 0)
				Array.Copy(items, 0, array, index, array.Length - index);
		}

		void ICollection<string>.CopyTo(string[] array, int index)
		{
			var items = this.Database.SetMembers(this.Name);

			if(items != null && items.Length > 0)
				Array.Copy(items, 0, array, index, array.Length - index);
		}

		int IList.Add(object value)
		{
			if(value == null)
				throw new ArgumentNullException(nameof(value));

			this.Database.SetAdd(this.Name, this.GetStoredValue(value));

			return -1;
		}

		bool IList.Contains(object value)
		{
			if(value == null)
				return false;

			return this.Contains(this.GetStoredValue(value));
		}

		int IList.IndexOf(object value)
		{
			throw new NotSupportedException();
		}

		void IList.Insert(int index, object value)
		{
			throw new NotSupportedException();
		}

		void IList.Remove(object value)
		{
			if(value == null)
				return;

			this.Database.SetRemove(this.Name, this.GetStoredValue(value));
		}

		void IList.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		object IList.this[int index]
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		#endregion

		#region 遍历枚举

		public IEnumerator<string> GetEnumerator()
		{
			return this.Database.SetScan(this.Name).Select(p => p.ToString()).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion

		#region 私有方法

		private RedisKey[] GetRedisKeys(string[] keys)
		{
			if(keys == null || keys.Length == 0)
				throw new ArgumentNullException(nameof(keys));

			var result = new RedisKey[keys.Length + 1];
			result[0] = this.Name;
			Array.Copy(keys, 0, result, 1, keys.Length);
			return result;
		}

		#endregion
	}
}