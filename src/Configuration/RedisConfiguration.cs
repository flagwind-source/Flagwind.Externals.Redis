﻿using System;

using Flagwind.Options.Configuration;

namespace Flagwind.Externals.Redis.Configuration
{
	public class RedisConfiguration : OptionConfigurationElement, IRedisConfiguration
	{
		#region 常量定义

		private const string XML_CONNECTIONPATH_ATTRIBUTE = "connectionPath";

		#endregion

		#region 公共属性

		[OptionConfigurationProperty(XML_CONNECTIONPATH_ATTRIBUTE, Behavior = OptionConfigurationPropertyBehavior.IsRequired)]
		public string ConnectionPath
		{
			get
			{
				return (string)this[XML_CONNECTIONPATH_ATTRIBUTE];
			}
			set
			{
				if(string.IsNullOrWhiteSpace(value))
					throw new ArgumentNullException();

				this[XML_CONNECTIONPATH_ATTRIBUTE] = value;
			}
		}

		public string ConnectionString
		{
			get
			{
				if(string.IsNullOrWhiteSpace(this.ConnectionPath))
					return null;

				var connectionString = Flagwind.ComponentModel.ApplicationContextBase.Current.OptionManager.GetOptionValue(this.ConnectionPath) as ConnectionStringElement;

				if(connectionString == null)
					throw new OptionConfigurationException($"Not found connection string by '{this.ConnectionPath}' configuration path.");

				return connectionString.Value;
			}
		}

		#endregion
	}
}