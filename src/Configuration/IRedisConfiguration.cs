﻿using System;

namespace Flagwind.Externals.Redis.Configuration
{
	public interface IRedisConfiguration
	{
		string ConnectionString
		{
			get;
		}
	}
}
