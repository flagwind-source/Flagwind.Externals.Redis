﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class HashsetClearCommand : RedisCommandBase
	{
		#region 构造方法

		public HashsetClearCommand() : base("Clear")
		{
		}

		public HashsetClearCommand(IRedisService redis) : base(redis, "Clear")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			foreach(var arg in context.Expression.Arguments)
			{
				var hashset = this.Redis.GetHashset(arg);

				if(hashset == null)
					context.Error.WriteLine($"The '{arg}' hashset is not existed.");
				else
					hashset.Clear();
			}

			return null;
		}

		#endregion
	}
}