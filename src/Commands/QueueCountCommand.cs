﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class QueueCountCommand : RedisCommandBase
	{
		#region 构造方法

		public QueueCountCommand() : base("Count")
		{
		}

		public QueueCountCommand(IRedisService redis) : base(redis, "Count")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			var result = new long[context.Expression.Arguments.Length];

			for(int i = 0; i < result.Length; i++)
			{
				var queue = this.Redis.GetQueue(context.Expression.Arguments[i]);

				if(queue == null)
				{
					context.Error.WriteLine($"The '{context.Expression.Arguments[i]}' queue is not existed.");
					return 0;
				}

				result[i] = queue.Count;
				context.Output.WriteLine(result[i].ToString());
			}

			if(result.Length == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}