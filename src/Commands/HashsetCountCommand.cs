﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class HashsetCountCommand : RedisCommandBase
	{
		#region 构造方法

		public HashsetCountCommand() : base("Count")
		{
		}

		public HashsetCountCommand(IRedisService redis) : base(redis, "Count")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			var result = new List<long>(context.Expression.Arguments.Length);

			for(int i = 0; i < context.Expression.Arguments.Length; i++)
			{
				var hashset = this.Redis.GetHashset(context.Expression.Arguments[i]);

				if(hashset == null)
				{
					context.Error.WriteLine($"The '{context.Expression.Arguments[i]}' hashset is not existed.");
					return 0;
				}

				result.Add(hashset.Count);
				context.Output.WriteLine(result[i].ToString());
			}

			if(result.Count == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}