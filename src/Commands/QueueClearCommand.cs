﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class QueueClearCommand : RedisCommandBase
	{
		#region 构造方法

		public QueueClearCommand() : base("Clear")
		{
		}

		public QueueClearCommand(IRedisService redis) : base(redis, "Clear")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			foreach(var arg in context.Expression.Arguments)
			{
				var queue = this.Redis.GetQueue(arg);

				if(queue == null)
					context.Error.WriteLine($"The '{arg}' queue is not existed.");
				else
					queue.Clear();
			}

			return null;
		}

		#endregion
	}
}