﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class FindCommand : RedisCommandBase
	{
		#region 构造方法

		public FindCommand() : base("Find")
		{
		}

		public FindCommand(IRedisService redis) : base(redis, "Find")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length == 0)
				throw new Flagwind.Services.CommandException("Missing arguments.");

			var result = new IEnumerable<string>[context.Expression.Arguments.Length];

			for(int i = 0; i < context.Expression.Arguments.Length; i++)
			{
				//查找指定模式的键名集
				result[i] = this.Redis.Find(context.Expression.Arguments[i]);

				//打印模式字符串
				context.Output.WriteLine(Services.CommandOutletColor.Magenta, context.Expression.Arguments[i]);

				//定义遍历序号
				var index = 1;

				foreach(var key in result[i])
				{
					context.Output.Write(Services.CommandOutletColor.DarkGray, $"[{index++}] ");
					context.Output.WriteLine(Services.CommandOutletColor.Green, key);
				}

				context.Output.WriteLine();
			}

			return result;
		}

		#endregion
	}
}