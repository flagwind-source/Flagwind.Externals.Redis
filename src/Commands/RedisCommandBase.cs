﻿using System;

namespace Flagwind.Externals.Redis.Commands
{
	public abstract class RedisCommandBase : Flagwind.Services.CommandBase<Flagwind.Services.CommandContext>
	{
		#region 成员字段

		private IRedisService _redis;

		#endregion

		#region 构造方法

		protected RedisCommandBase(string name) : base(name)
		{
		}

		protected RedisCommandBase(IRedisService redis, string name) : base(name)
		{
			_redis = redis;
		}

		#endregion

		#region 公共属性

		public IRedisService Redis
		{
			get
			{
				return _redis;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException();

				_redis = value;
			}
		}

		#endregion

		#region 重写方法

		public override bool CanExecute(Services.CommandContext parameter)
		{
			return _redis != null && base.CanExecute(parameter);
		}

		#endregion
	}
}