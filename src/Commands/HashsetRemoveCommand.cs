﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class HashsetRemoveCommand : RedisCommandBase
	{
		#region 构造方法

		public HashsetRemoveCommand() : base("Remove")
		{
		}

		public HashsetRemoveCommand(IRedisService redis) : base(redis, "Remove")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 2)
				throw new Services.CommandException("Missing arguments.");

			var hashset = this.Redis.GetHashset(context.Expression.Arguments[0]);

			if(hashset == null)
			{
				context.Error.WriteLine($"The '{context.Expression.Arguments[0]}' hashset is not existed.");
				return null;
			}

			if(context.Expression.Arguments.Length == 2)
				return hashset.Remove(context.Expression.Arguments[1]) ? 1 : 0;
			else
			{
				var items = new string[context.Expression.Arguments.Length - 1];
				Array.Copy(context.Expression.Arguments, 1, items, 0, items.Length);

				return hashset.RemoveRange(items);
			}
		}

		#endregion
	}
}