﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class RemoveCommand : RedisCommandBase
	{
		#region 构造方法

		public RemoveCommand() : base("Remove")
		{
		}

		public RemoveCommand(IRedisService redis) : base(redis, "Remove")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Flagwind.Services.CommandException("Invalid arguments of command.");

			if(context.Expression.Arguments.Length == 1)
				this.Redis.Remove(context.Expression.Arguments[0]);
			else
				this.Redis.RemoveMany(context.Expression.Arguments);

			return null;
		}

		#endregion
	}
}