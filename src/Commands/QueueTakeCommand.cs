﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	[Flagwind.Services.CommandOption("index", Type = typeof(int), Description = "${Text.ListCommand.Index}")]
	[Flagwind.Services.CommandOption("count", Type = typeof(int), DefaultValue = 1, Description = "${Text.ListCommand.Count}")]
	public class QueueTakeCommand : RedisCommandBase
	{
		#region 构造方法

		public QueueTakeCommand() : base("Take")
		{
		}

		public QueueTakeCommand(IRedisService redis) : base(redis, "Take")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Flagwind.Services.CommandException("Missing arguments.");

			var count = context.Expression.Options.GetValue<int>("count");
			var index = context.Expression.Options.GetValue<int>("index");
			var result = new List<string>(context.Expression.Arguments.Length * count);

			for(int i = 0; i < context.Expression.Arguments.Length; i++)
			{
				var queue = this.Redis.GetQueue(context.Expression.Arguments[0]);

				if(queue == null)
				{
					context.Error.WriteLine($"The '{context.Expression.Arguments[i]}' queue is not existed.");
					return null;
				}

				//打印当前队列名
				context.Output.WriteLine(Services.CommandOutletColor.Magenta, $"Dequeued entries from 'context.Expression.Arguments[i]' queue:");

				var items = queue.Take(index, count);

				foreach(var item in items)
				{
					result.Add((string)item);
					context.Output.WriteLine(item);
				}

				context.Output.WriteLine();
			}

			return result;
		}

		#endregion
	}
}