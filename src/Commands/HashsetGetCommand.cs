﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class HashsetGetCommand : RedisCommandBase
	{
		#region 构造方法

		public HashsetGetCommand() : base("Get")
		{
		}

		public HashsetGetCommand(IRedisService redis) : base(redis, "Get")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			var result = new IRedisHashset[context.Expression.Arguments.Length];

			for(var i = 0; i < context.Expression.Arguments.Length; i++)
			{
				result[i] = this.Redis.GetHashset(context.Expression.Arguments[i]);

				if(result[i] == null)
				{
					context.Error.WriteLine($"The '{context.Expression.Arguments[i]}' hashset is not existed.");
					return null;
				}

				context.Output.WriteLine(Services.CommandOutletColor.Magenta, $"The '{context.Expression.Arguments[i]}' hashset have {result[i].Count} entries:");

				foreach(var item in result[i])
					context.Output.WriteLine(item.ToString());

				context.Output.WriteLine();
			}

			if(result.Length == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}