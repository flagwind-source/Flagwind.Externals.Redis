﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class DictionaryRemoveCommand : RedisCommandBase
	{
		#region 构造方法

		public DictionaryRemoveCommand() : base("Remove")
		{
		}

		public DictionaryRemoveCommand(IRedisService redis) : base(redis, "Remove")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 2)
				throw new Flagwind.Services.CommandException("Missing arguments.");

			var dictionary = this.Redis.GetDictionary(context.Expression.Arguments[0]);

			if(dictionary == null)
			{
				context.Error.WriteLine($"The '{context.Expression.Arguments[0]}' dictionary is not existed.");
				return 0;
			}

			int count = 0;

			for(int i = 1; i < context.Expression.Arguments.Length; i++)
			{
				count += dictionary.Remove(context.Expression.Arguments[i]) ? 1 : 0;
			}

			return count;
		}

		#endregion
	}
}