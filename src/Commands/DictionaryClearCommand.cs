﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class DictionaryClearCommand : RedisCommandBase
	{
		#region 构造方法

		public DictionaryClearCommand() : base("Clear")
		{
		}

		public DictionaryClearCommand(IRedisService redis) : base(redis, "Clear")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			foreach(var arg in context.Expression.Arguments)
			{
				var dictionary = this.Redis.GetDictionary(arg);

				if(dictionary == null)
					context.Error.WriteLine($"The '{arg}' dictionary is not existed.");
				else
					dictionary.Clear();
			}

			return null;
		}

		#endregion
	}
}