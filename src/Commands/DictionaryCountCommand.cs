﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class DictionaryCountCommand : RedisCommandBase
	{
		#region 构造方法

		public DictionaryCountCommand() : base("Count")
		{
		}

		public DictionaryCountCommand(IRedisService redis) : base(redis, "Count")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Services.CommandException("Missing arguments.");

			var result = new long[context.Expression.Arguments.Length];

			for(int i = 0; i < result.Length; i++)
			{
				var dictionary = this.Redis.GetDictionary(context.Expression.Arguments[i]);

				if(dictionary == null)
				{
					context.Error.WriteLine($"The '{context.Expression.Arguments[i]}' dictionary is not existed.");
					return 0;
				}

				result[i] = dictionary.Count;
				context.Output.WriteLine(result[i].ToString());
			}

			if(result.Length == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}