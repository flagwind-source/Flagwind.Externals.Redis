﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	[Flagwind.Services.CommandOption("seed", Type = typeof(int), DefaultValue = 0)]
	[Flagwind.Services.CommandOption("interval", Type = typeof(int), DefaultValue = 1)]
	public class DecrementCommand : RedisCommandBase
	{
		#region 构造方法

		public DecrementCommand() : base("Decrement")
		{
		}

		public DecrementCommand(IRedisService redis) : base(redis, "Decrement")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Flagwind.Services.CommandException("Missing arguments.");

			int seed = context.Expression.Options.GetValue<int>("seed");
			var interval = context.Expression.Options.GetValue<int>("interval");
			var result = new long[context.Expression.Arguments.Length];

			for(int i = 0; i < context.Expression.Arguments.Length; i++)
			{
				result[i] = this.Redis.Decrement(context.Expression.Arguments[i], interval, seed);
				context.Output.WriteLine(result[i].ToString());
			}

			if(result.Length == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}