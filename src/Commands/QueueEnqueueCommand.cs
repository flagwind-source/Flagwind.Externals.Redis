﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class QueueEnqueueCommand : RedisCommandBase
	{
		#region 构造方法

		public QueueEnqueueCommand() : base("In")
		{
		}

		public QueueEnqueueCommand(IRedisService redis) : base(redis, "In")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 2)
				throw new Services.CommandException("Missing arguments.");

			var queue = this.Redis.GetQueue(context.Expression.Arguments[0]);

			if(queue == null)
			{
				context.Error.WriteLine($"The '{context.Expression.Arguments[0]}' queue is not existed.");
				return 0;
			}

			if(context.Expression.Arguments.Length == 2)
			{
				queue.Enqueue(context.Expression.Arguments[1]);
				return 1;
			}
			else
			{
				var items = new string[context.Expression.Arguments.Length - 1];
				Array.Copy(context.Expression.Arguments, 1, items, 0, items.Length);

				return queue.EnqueueMany(items);
			}
		}

		#endregion
	}
}