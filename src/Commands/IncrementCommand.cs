﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	[Flagwind.Services.CommandOption("seed", Type = typeof(int), DefaultValue = 0)]
	[Flagwind.Services.CommandOption("interval", Type = typeof(int), DefaultValue = 1)]
	public class IncrementCommand : RedisCommandBase
	{
		#region 构造方法

		public IncrementCommand() : base("Increment")
		{
		}

		public IncrementCommand(IRedisService redis) : base(redis, "Increment")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new Flagwind.Services.CommandException("Missing arguments.");

			int seed = context.Expression.Options.GetValue<int>("seed");
			int interval = context.Expression.Options.GetValue<int>("interval");
			var result = new long[context.Expression.Arguments.Length];

			for(int i = 0; i < context.Expression.Arguments.Length; i++)
			{
				result[i] = this.Redis.Increment(context.Expression.Arguments[i], interval, seed);
				context.Output.WriteLine(result[i].ToString());
			}

			if(result.Length == 1)
				return result[0];
			else
				return result;
		}

		#endregion
	}
}