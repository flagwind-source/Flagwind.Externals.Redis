﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis.Commands
{
	public class CountCommand : RedisCommandBase
	{
		#region 构造方法

		public CountCommand() : base("Count")
		{
		}

		public CountCommand(IRedisService redis) : base(redis, "Count")
		{
		}

		#endregion

		#region 执行方法

		protected override object OnExecute(Services.CommandContext context)
		{
			var count = this.Redis.Count;
			context.Output.WriteLine(count.ToString());
			return count;
		}

		#endregion
	}
}