﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Redis
{
	public abstract class RedisObjectBase : MarshalByRefObject
	{
		#region 成员字段

		private string _name;
		private StackExchange.Redis.IDatabase _database;

		#endregion

		#region 构造方法

		protected RedisObjectBase(string name, StackExchange.Redis.IDatabase database)
		{
			if(string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			if(database == null)
				throw new ArgumentNullException(nameof(database));

			_name = name.Trim();
			_database = database;
		}

		#endregion

		#region 公共属性

		/// <summary>
		/// 获取或设置Redis对象的名称。
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if(string.IsNullOrWhiteSpace(value))
					throw new ArgumentNullException();

				_name = value.Trim();
			}
		}

		#endregion

		#region 保护属性

		/// <summary>
		/// 获取或设置当前Redis对象所依附的Redis数据库。
		/// </summary>
		protected StackExchange.Redis.IDatabase Database
		{
			get
			{
				return _database;
			}
		}

		#endregion

		#region 虚拟方法

		protected virtual StackExchange.Redis.RedisValue GetStoredValue(object value)
		{
			return Utility.GetStoredValue(value);
		}

		#endregion
	}
}