﻿using System;
using System.Linq;
using System.Collections.Generic;

using StackExchange.Redis;

namespace Flagwind.Externals.Redis
{
    internal static class RedisValueExtension
    {
        public static RedisKey[] ToRedisKeys(this IEnumerable<string> keys)
        {
            if(keys == null)
                throw new ArgumentNullException(nameof(keys));

            return keys.Select(p => (RedisKey)p).ToArray();
        }

        public static RedisValue[] ToRedisValues(this IEnumerable<string> values)
        {
            if(values == null)
                throw new ArgumentNullException(nameof(values));

            return values.Select(p => (RedisValue)p).ToArray();
        }

        public static string[] ToStringArray(this RedisValue[] values)
        {
            return ExtensionMethods.ToStringArray(values);
        }
    }
}
