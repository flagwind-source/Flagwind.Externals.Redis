﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Externals.Redis.Tests")]
[assembly: AssemblyDescription("This is a library about Redis SDK.")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind.Externals.Redis Library")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]
[assembly: ComVisible(false)]
[assembly: Guid("21fe0243-cfff-4998-8848-cb08032027f9")]

[assembly: AssemblyVersion("1.3.3.623")]
[assembly: AssemblyFileVersion("1.3.3.623")]