﻿using System;
using System.Collections;
using System.Collections.Generic;
using Flagwind.Common;
using Flagwind.Runtime.Caching;

using Xunit;

namespace Flagwind.Externals.Redis.Tests
{
    public class CacheTest
    {
        #region 私有变量

        private ICache _storage;

        #endregion

        #region 构造方法

        public CacheTest()
        {
            _storage = new RedisService("127.0.0.1:6379");
        }

        #endregion

        #region 测试方法

        [Fact]
        public void CollectionTest()
        {
            var names = this._storage.GetValue<ICollection<string>>("Flagwind.Externals.Redis.Tests.Names");

            if(names != null)
            {
                for(var i = 0; i < 10; i++)
                {
                    names.Add($"Name-{i}");
                }
            }
            else
            {
                this._storage.SetValue("Flagwind.Externals.Redis.Tests.Names:LALA", new List<string>[]{});
            }

        }

        #endregion
    }
}
